# gridcraft
Gridcraft is a 2D turn-based strategy survival game. Move your character around a chessboard-like procedurally generated grid, gather loot, fight enemies, and try to survive.

# Project diary
## March 10, 2024
### Current features
A procedurally generated map with several different biomes has been created. The player can click their character to reveal their legal moves and click one to move there, illegal moves are ignored. The player is slowed considerably by water.
### Next steps
Implementing save files so that gameplay can persist between sessions seems like a good next step. Not too hard, I think...just pickle the world map. 

Terrain features and obstacles like trees and rocks can be implemented as well. Seems more fun than a savefile.