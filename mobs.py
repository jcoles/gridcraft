class Player():

    def __init__(self,name,spawnX,spawnY):
        self.name = name
        self.health = 100
        self.stamina = 200
        self.inventory = [None,None,None,None,None,None,None,None,None]
        self.hand_item = None
        self.armor = None
        self.helmet = None
        self.x = spawnX
        self.y = spawnY
