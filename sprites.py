import pygame
class PlayerSprite(pygame.sprite.Sprite):

    def __init__(self,playerEntity,c):
        super().__init__()
        self.playerEntity = playerEntity
        self.image = pygame.image.load("assets/player_x.png")
        self.rect = self.image.get_rect(center=c)
