import tkinter as tk
from cell import *
from random import random, choice
from rtree import index
from mobs import *
from sprites import *

def load_tiles(x1,y1,x2,y2):
    for x in range(x1,x2):
        for y in range(y1,y2):
            if world_map.get((x,y),None) is not None:
                continue
            if random() < VORONOI_PROBABILITY:
                can_add_biome = True
                nearest_vp = vp_map[list(voronoi_points.nearest((x,y,x,y),1))[0]]
                if abs(nearest_vp[0]-x)+abs(nearest_vp[1]-y) < BIOME_THRESHOLD:
                    can_add_biome = False
                    break
                if can_add_biome:
                    votes = {}
                    neighbors = list(voronoi_points.nearest((x,y,x,y),5))
                    for n in neighbors:
                        vote = Biome.vote(world_map[vp_map[n]].biome)
                        votes[vote] = votes.get(vote,0)+1


                    chosen_biome = choice(list(Biome))
                    if len(votes) > 0:
                        chosen_biome = sorted(list(votes.items()),key=lambda a:a[1],reverse=True)[0][0]

                    id = len(voronoi_points)
                    voronoi_points.insert(id,(x,y,x,y))
                    vp_map[id] = (x,y)
                    world_map[(x,y)] = Cell(chosen_biome)

    for x in range(x1,x2):
        for y in range(y1,y2):
            if world_map.get((x,y),None) is not None:
                continue
            world_map[(x,y)] = Cell(world_map[vp_map[list(voronoi_points.nearest((x,y,x,y),1))[0]]].biome)


def get_legal_moves(p):
    legal_moves = []
    for t in [(1,0),(-1,0),(0,1),(0,-1),(1,1),(1,-1),(-1,1),(-1,-1)]:
        for i in range(1,5):
            new_tile = (p.x+i*t[0],p.y+i*t[1])
            print(new_tile)
            if world_map[new_tile].feature is not None or world_map[new_tile].entity is not None:
                break
            legal_moves += [(i*t[0],i*t[1])]
            if(world_map[new_tile].biome == Biome.LAKE):
                break
    return legal_moves

if __name__ == "__main__":
    import pygame
    from pygame import Rect, Color
    VIEW_DISTANCE = 31
    VORONOI_PROBABILITY = 0.01 #0.00025
    BIOME_THRESHOLD = 10
    voronoi_points = {(0,0):set((0,0))}
    voronoi_points = index.Index()
    voronoi_points.insert(0,(0,0,0,0))
    vp_map = {0:(0,0)}
    world_map = {}

    spawn_biome = Biome.LAKE
    while spawn_biome == Biome.LAKE:
        spawn_biome = choice(list(Biome))


    world_map[(0,0)] = Cell(spawn_biome)
    color_map = {Biome.PLAINS:(0,192,0),
                 Biome.DECIDUOUS_FOREST:(0,128,0),
                 Biome.LAKE:(0,0,255),
                 Biome.DESERT:(255,255,0),
                 Biome.BADLANDS:(192,64,0),
                 Biome.CONIFEROUS_FOREST:(0,96,32),
                 Biome.TUNDRA:(224,224,224)}
    # pygame setup
    pygame.init()
    screen = pygame.display.set_mode((775,775))
    clock = pygame.time.Clock()
    running = True
    load_tiles(-15,-15,16,16)
    p = Player("Jakob",0,0)
    all_sprites = pygame.sprite.Group()
    ps = PlayerSprite(p,screen.get_rect().center)


    all_sprites.add(ps)
    world_map[(0,0)].entity = p
    legal_moves = []
    while running:

        for i in range(p.x-15,p.x+16):
            for j in range(p.y-15,p.y+16):
                bb_x = (i-p.x+15)*25
                bb_y = (j-p.y+15)*25
                pygame.draw.rect(screen,Color(color_map[world_map[(i,j)].biome]),Rect(bb_x,bb_y,bb_x+25,bb_y+25))
                pygame.draw.rect(screen,"black",Rect(bb_x,bb_y,bb_x+25,bb_y+25),width=1)
        all_sprites.update()
        all_sprites.draw(screen)
        for move in legal_moves:
            pygame.draw.circle(screen,"red",pygame.Vector2((cell[0]+move[0])*25+12,(cell[1]+move[1])*25+12),radius=10,width=2)
        pygame.display.flip()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                cell =  (pos[0]//25,pos[1]//25)
                legal_cells = [(15+c[0],15+c[1]) for c in legal_moves]
                if cell==(15,15):
                    if len(legal_moves):
                        legal_moves = []
                    else:
                        legal_moves = get_legal_moves(p)
                    print(legal_moves)
                elif cell in legal_cells:
                    p.x += cell[0]-15
                    p.y += cell[1]-15
                    load_tiles(p.x-15,p.y-15,p.x+16,p.y+16)
                    legal_moves = []
                else:
                    legal_moves = []


    pygame.quit()
