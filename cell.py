from enum import Enum
from random import choice
class Biome(Enum):
    PLAINS = 0
    DECIDUOUS_FOREST = 1
    DESERT = 2
    LAKE = 3
    BADLANDS = 4
    TUNDRA = 5
    CONIFEROUS_FOREST = 6

    @classmethod
    def vote(cls,biome):
        likes = {cls.PLAINS:[cls.DECIDUOUS_FOREST,cls.DESERT,cls.LAKE,cls.BADLANDS,cls.CONIFEROUS_FOREST],
                 cls.DECIDUOUS_FOREST:[cls.PLAINS,cls.LAKE,cls.CONIFEROUS_FOREST],
                 cls.DESERT:[cls.PLAINS,cls.BADLANDS],
                 cls.LAKE:[cls.PLAINS,cls.DECIDUOUS_FOREST,cls.TUNDRA,cls.CONIFEROUS_FOREST],
                 cls.BADLANDS:[cls.PLAINS,cls.DESERT],
                 cls.TUNDRA:[cls.LAKE,cls.CONIFEROUS_FOREST],
                 cls.CONIFEROUS_FOREST:[cls.PLAINS,cls.LAKE,cls.DECIDUOUS_FOREST,cls.TUNDRA]}
        return choice(likes[biome])

class Tile(Enum):
    GRASS = 0
    SAND = 1
    WATER = 2
    RED_SAND = 3
    ROCK = 4
    SNOW = 5



class Cell:
    def __init__(self,biome):
        self.biome = biome
        self.feature  = None
        self.entity = None
        self.feature = None
        self.isVoronoiPoint = False
